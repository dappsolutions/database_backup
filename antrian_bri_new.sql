-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.0.67-community-nt - MySQL Community Edition (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4235
-- Date/time:                    2020-11-01 17:16:04
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for antrian_bri
DROP DATABASE IF EXISTS `antrian_bri`;
CREATE DATABASE IF NOT EXISTS `antrian_bri` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `antrian_bri`;


-- Dumping structure for table antrian_bri.cek_error
DROP TABLE IF EXISTS `cek_error`;
CREATE TABLE IF NOT EXISTS `cek_error` (
  `jmlh_error` int(11) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table antrian_bri.cek_error: 1 rows
/*!40000 ALTER TABLE `cek_error` DISABLE KEYS */;
REPLACE INTO `cek_error` (`jmlh_error`) VALUES
	(0);
/*!40000 ALTER TABLE `cek_error` ENABLE KEYS */;


-- Dumping structure for table antrian_bri.setvol
DROP TABLE IF EXISTS `setvol`;
CREATE TABLE IF NOT EXISTS `setvol` (
  `volx` int(11) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table antrian_bri.setvol: 1 rows
/*!40000 ALTER TABLE `setvol` DISABLE KEYS */;
REPLACE INTO `setvol` (`volx`) VALUES
	(-3000);
/*!40000 ALTER TABLE `setvol` ENABLE KEYS */;


-- Dumping structure for table antrian_bri.tidak_puas
DROP TABLE IF EXISTS `tidak_puas`;
CREATE TABLE IF NOT EXISTS `tidak_puas` (
  `id_tidak_puas` int(11) default NULL,
  `nama_tidak_puas` varchar(255) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table antrian_bri.tidak_puas: 0 rows
/*!40000 ALTER TABLE `tidak_puas` DISABLE KEYS */;
/*!40000 ALTER TABLE `tidak_puas` ENABLE KEYS */;


-- Dumping structure for table antrian_bri.tprint
DROP TABLE IF EXISTS `tprint`;
CREATE TABLE IF NOT EXISTS `tprint` (
  `kd` int(11) default NULL,
  `loket` varchar(15) default NULL,
  `no` int(11) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table antrian_bri.tprint: 0 rows
/*!40000 ALTER TABLE `tprint` DISABLE KEYS */;
/*!40000 ALTER TABLE `tprint` ENABLE KEYS */;


-- Dumping structure for table antrian_bri.t_absen
DROP TABLE IF EXISTS `t_absen`;
CREATE TABLE IF NOT EXISTS `t_absen` (
  `tanggal` date default NULL,
  `loket` varchar(25) default NULL,
  `nama` varchar(35) default NULL,
  `status` varchar(25) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table antrian_bri.t_absen: 0 rows
/*!40000 ALTER TABLE `t_absen` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_absen` ENABLE KEYS */;


-- Dumping structure for table antrian_bri.t_antri
DROP TABLE IF EXISTS `t_antri`;
CREATE TABLE IF NOT EXISTS `t_antri` (
  `tgl` date default NULL,
  `id_tidak_puas` int(11) default NULL,
  `no_antri` int(11) default NULL,
  `loket` varchar(7) default NULL,
  `nama` varchar(15) default NULL,
  `ket` varchar(2) default NULL,
  `ket_cs` varchar(2) default NULL,
  `msk` time default NULL,
  `klr` time default NULL,
  `lm` int(11) default NULL,
  `ke` varchar(3) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table antrian_bri.t_antri: 6 rows
/*!40000 ALTER TABLE `t_antri` DISABLE KEYS */;
REPLACE INTO `t_antri` (`tgl`, `id_tidak_puas`, `no_antri`, `loket`, `nama`, `ket`, `ket_cs`, `msk`, `klr`, `lm`, `ke`) VALUES
	('2020-10-15', NULL, 1, 'Teller', NULL, '0', '2', NULL, NULL, NULL, '0'),
	('2020-11-01', 99, 800, 'CS', 'Teller 3', '2', '2', '17:01:08', NULL, 1, '3'),
	('2020-10-15', NULL, 2, 'Teller', NULL, '0', '2', NULL, NULL, NULL, '0'),
	('2020-10-15', NULL, 801, 'CS', NULL, '0', '2', NULL, NULL, NULL, '0'),
	('2020-11-01', 99, 800, 'CS', 'Teller 3', '2', '2', '17:01:08', NULL, 1, '3'),
	('2020-11-01', 99, 801, 'CS', 'Customer Servic', '2', '2', '16:59:44', NULL, NULL, '2');
/*!40000 ALTER TABLE `t_antri` ENABLE KEYS */;


-- Dumping structure for table antrian_bri.t_data
DROP TABLE IF EXISTS `t_data`;
CREATE TABLE IF NOT EXISTS `t_data` (
  `uker` varchar(150) default NULL,
  `almt` varchar(75) default NULL,
  `tlp` varchar(25) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table antrian_bri.t_data: 1 rows
/*!40000 ALTER TABLE `t_data` DISABLE KEYS */;
REPLACE INTO `t_data` (`uker`, `almt`, `tlp`) VALUES
	('POLRES TRENGGALEK', 'TRENGGALEK', '-');
/*!40000 ALTER TABLE `t_data` ENABLE KEYS */;


-- Dumping structure for table antrian_bri.t_display
DROP TABLE IF EXISTS `t_display`;
CREATE TABLE IF NOT EXISTS `t_display` (
  `bungax` int(11) default NULL,
  `kursx` int(11) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table antrian_bri.t_display: 1 rows
/*!40000 ALTER TABLE `t_display` DISABLE KEYS */;
REPLACE INTO `t_display` (`bungax`, `kursx`) VALUES
	(1, 1);
/*!40000 ALTER TABLE `t_display` ENABLE KEYS */;


-- Dumping structure for table antrian_bri.t_greeting
DROP TABLE IF EXISTS `t_greeting`;
CREATE TABLE IF NOT EXISTS `t_greeting` (
  `pagi` time default NULL,
  `siang` time default NULL,
  `siang2` time default NULL,
  `sore` time default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table antrian_bri.t_greeting: 1 rows
/*!40000 ALTER TABLE `t_greeting` DISABLE KEYS */;
REPLACE INTO `t_greeting` (`pagi`, `siang`, `siang2`, `sore`) VALUES
	('07:30:00', '12:00:00', '13:00:00', '16:30:00');
/*!40000 ALTER TABLE `t_greeting` ENABLE KEYS */;


-- Dumping structure for table antrian_bri.t_history
DROP TABLE IF EXISTS `t_history`;
CREATE TABLE IF NOT EXISTS `t_history` (
  `tgl` date default NULL,
  `cs` int(11) default NULL,
  `teller` int(11) default NULL,
  `cs_on` int(11) default NULL,
  `teller_on` int(11) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table antrian_bri.t_history: 2 rows
/*!40000 ALTER TABLE `t_history` DISABLE KEYS */;
REPLACE INTO `t_history` (`tgl`, `cs`, `teller`, `cs_on`, `teller_on`) VALUES
	('2020-10-15', NULL, 2, 1, NULL),
	('2020-11-01', NULL, 1, 3, NULL);
/*!40000 ALTER TABLE `t_history` ENABLE KEYS */;


-- Dumping structure for table antrian_bri.t_jingle
DROP TABLE IF EXISTS `t_jingle`;
CREATE TABLE IF NOT EXISTS `t_jingle` (
  `jinglex` time default NULL,
  `jmlh` int(11) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table antrian_bri.t_jingle: 1 rows
/*!40000 ALTER TABLE `t_jingle` DISABLE KEYS */;
REPLACE INTO `t_jingle` (`jinglex`, `jmlh`) VALUES
	('07:00:00', 2);
/*!40000 ALTER TABLE `t_jingle` ENABLE KEYS */;


-- Dumping structure for table antrian_bri.t_kurs
DROP TABLE IF EXISTS `t_kurs`;
CREATE TABLE IF NOT EXISTS `t_kurs` (
  `negara` varchar(25) default NULL,
  `beli` decimal(20,2) default NULL,
  `jual` decimal(20,2) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table antrian_bri.t_kurs: 6 rows
/*!40000 ALTER TABLE `t_kurs` DISABLE KEYS */;
REPLACE INTO `t_kurs` (`negara`, `beli`, `jual`) VALUES
	('USD', 13525.00, 13725.00),
	('EUR', 14841.00, 15202.00),
	('SAR', 3794.00, 3794.00),
	('SGD', 9610.00, 9867.00),
	('JPY', 111.33, 114.96),
	('AUD', 9564.00, 9880.00);
/*!40000 ALTER TABLE `t_kurs` ENABLE KEYS */;


-- Dumping structure for table antrian_bri.t_loket
DROP TABLE IF EXISTS `t_loket`;
CREATE TABLE IF NOT EXISTS `t_loket` (
  `no` varchar(2) default NULL,
  `loket` varchar(25) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table antrian_bri.t_loket: 10 rows
/*!40000 ALTER TABLE `t_loket` DISABLE KEYS */;
REPLACE INTO `t_loket` (`no`, `loket`) VALUES
	('01', 'Teller 1'),
	('02', 'Teller 2'),
	('03', 'Teller 3'),
	('04', 'Teller 4'),
	('05', 'Teller 5'),
	('06', 'Customer Service 1'),
	('07', 'Customer Service 2'),
	('08', 'Customer Service 3'),
	('09', 'Customer Service 4'),
	('10', 'Customer Service 5');
/*!40000 ALTER TABLE `t_loket` ENABLE KEYS */;


-- Dumping structure for table antrian_bri.t_mars
DROP TABLE IF EXISTS `t_mars`;
CREATE TABLE IF NOT EXISTS `t_mars` (
  `marsx` time default NULL,
  `jmlh` int(11) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table antrian_bri.t_mars: 1 rows
/*!40000 ALTER TABLE `t_mars` DISABLE KEYS */;
REPLACE INTO `t_mars` (`marsx`, `jmlh`) VALUES
	('07:20:00', 1);
/*!40000 ALTER TABLE `t_mars` ENABLE KEYS */;


-- Dumping structure for table antrian_bri.t_shutdown
DROP TABLE IF EXISTS `t_shutdown`;
CREATE TABLE IF NOT EXISTS `t_shutdown` (
  `matikan` int(11) default NULL,
  `ket` int(11) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table antrian_bri.t_shutdown: 1 rows
/*!40000 ALTER TABLE `t_shutdown` DISABLE KEYS */;
REPLACE INTO `t_shutdown` (`matikan`, `ket`) VALUES
	(0, 0);
/*!40000 ALTER TABLE `t_shutdown` ENABLE KEYS */;


-- Dumping structure for table antrian_bri.t_simpanan
DROP TABLE IF EXISTS `t_simpanan`;
CREATE TABLE IF NOT EXISTS `t_simpanan` (
  `kd_simpanan` int(11) default NULL,
  `jenis` varchar(50) default NULL,
  `nama` varchar(50) default NULL,
  `bunga` decimal(20,2) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table antrian_bri.t_simpanan: 18 rows
/*!40000 ALTER TABLE `t_simpanan` DISABLE KEYS */;
REPLACE INTO `t_simpanan` (`kd_simpanan`, `jenis`, `nama`, `bunga`) VALUES
	(1, 'deposito1', '1 – 2 Bulan', 4.25),
	(2, 'deposito2', '3 Bulan', 7.75),
	(3, 'deposito3', '6 Bulan', 7.75),
	(4, 'deposito4', '12 Bulan', 7.00),
	(5, 'deposito5', '18 Bulan', 6.25),
	(6, 'deposito6', '24 Bulan', 6.50),
	(7, 'britama1', '0 s/d < 500,000', 0.00),
	(8, 'britama2', '500,000 s/d 5 JT', 1.00),
	(9, 'britama3', '> 5 JT s/d 50 JT', 1.00),
	(10, 'britama4', '> 50 JT s/d 100 JT', 1.25),
	(11, 'britama5', '> 100 JT s/d 1 M', 1.50),
	(12, 'britama6', '> 1 Milyar', 1.25),
	(13, 'simpedes1', '0 s/d < 500,000', 0.00),
	(14, 'simpedes2', '500,000 s/d 5 JT', 1.00),
	(15, 'simpedes3', '> 5 JT s/d 50 JT', 1.00),
	(16, 'simpedes4', '> 50 Juta s/d 100 Juta', 1.25),
	(17, 'simpedes5', '-', 0.00),
	(18, 'simpedes6', '-', 0.00);
/*!40000 ALTER TABLE `t_simpanan` ENABLE KEYS */;


-- Dumping structure for table antrian_bri.t_text
DROP TABLE IF EXISTS `t_text`;
CREATE TABLE IF NOT EXISTS `t_text` (
  `kd_text` int(11) default NULL,
  `textNya` text,
  `statusx` int(11) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table antrian_bri.t_text: 3 rows
/*!40000 ALTER TABLE `t_text` DISABLE KEYS */;
REPLACE INTO `t_text` (`kd_text`, `textNya`, `statusx`) VALUES
	(1, '-', 0),
	(2, '-', 0),
	(3, '-', 0);
/*!40000 ALTER TABLE `t_text` ENABLE KEYS */;


-- Dumping structure for table antrian_bri.t_tnt
DROP TABLE IF EXISTS `t_tnt`;
CREATE TABLE IF NOT EXISTS `t_tnt` (
  `tnt` int(11) default NULL,
  `ket1` int(11) default NULL,
  `ket2` int(11) default NULL,
  `ket3` int(11) default NULL,
  `ket4` int(11) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table antrian_bri.t_tnt: 1 rows
/*!40000 ALTER TABLE `t_tnt` DISABLE KEYS */;
REPLACE INTO `t_tnt` (`tnt`, `ket1`, `ket2`, `ket3`, `ket4`) VALUES
	(0, 0, 0, 0, 0);
/*!40000 ALTER TABLE `t_tnt` ENABLE KEYS */;


-- Dumping structure for table antrian_bri.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `username` varchar(20) default NULL,
  `password` varchar(32) default NULL,
  `level` varchar(10) default NULL,
  `bagian` varchar(20) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table antrian_bri.user: 10 rows
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
REPLACE INTO `user` (`username`, `password`, `level`, `bagian`) VALUES
	('teller1', 'e10adc3949ba59abbe56e057f20f883e', 'teller', 'Teller 1'),
	('teller2', 'e10adc3949ba59abbe56e057f20f883e', 'teller', 'Teller 2'),
	('teller3', 'e10adc3949ba59abbe56e057f20f883e', 'teller', 'Teller 3'),
	('teller4', 'e10adc3949ba59abbe56e057f20f883e', 'teller', 'Teller 4'),
	('teller5', 'e10adc3949ba59abbe56e057f20f883e', 'teller', 'Teller 5'),
	('cs1', 'e10adc3949ba59abbe56e057f20f883e', 'cs', 'Customer Service 1'),
	('cs2', 'e10adc3949ba59abbe56e057f20f883e', 'cs', 'Customer Service 2'),
	('cs3', 'e10adc3949ba59abbe56e057f20f883e', 'cs', 'Customer Service 3'),
	('cs4', 'e10adc3949ba59abbe56e057f20f883e', 'cs', 'Customer Service 4'),
	('cs5', 'e10adc3949ba59abbe56e057f20f883e', 'cs', 'Customer Service 5');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
