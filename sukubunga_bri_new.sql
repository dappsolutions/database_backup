-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.0.67-community-nt - MySQL Community Edition (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4235
-- Date/time:                    2020-11-01 14:52:59
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for sukubunga_bri
DROP DATABASE IF EXISTS `sukubunga_bri`;
CREATE DATABASE IF NOT EXISTS `sukubunga_bri` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `sukubunga_bri`;


-- Dumping structure for table sukubunga_bri.cek_error
DROP TABLE IF EXISTS `cek_error`;
CREATE TABLE IF NOT EXISTS `cek_error` (
  `jmlh_error` int(11) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table sukubunga_bri.cek_error: 1 rows
/*!40000 ALTER TABLE `cek_error` DISABLE KEYS */;
REPLACE INTO `cek_error` (`jmlh_error`) VALUES
	(0);
/*!40000 ALTER TABLE `cek_error` ENABLE KEYS */;


-- Dumping structure for table sukubunga_bri.setvol
DROP TABLE IF EXISTS `setvol`;
CREATE TABLE IF NOT EXISTS `setvol` (
  `volx` int(11) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table sukubunga_bri.setvol: 1 rows
/*!40000 ALTER TABLE `setvol` DISABLE KEYS */;
REPLACE INTO `setvol` (`volx`) VALUES
	(10);
/*!40000 ALTER TABLE `setvol` ENABLE KEYS */;


-- Dumping structure for table sukubunga_bri.t_data
DROP TABLE IF EXISTS `t_data`;
CREATE TABLE IF NOT EXISTS `t_data` (
  `uker` varchar(150) default NULL,
  `almt` varchar(75) default NULL,
  `tlp` varchar(25) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table sukubunga_bri.t_data: 1 rows
/*!40000 ALTER TABLE `t_data` DISABLE KEYS */;
REPLACE INTO `t_data` (`uker`, `almt`, `tlp`) VALUES
	('BRI Unit Sidoarjo Kota 1', 'Sidoarjo', '-');
/*!40000 ALTER TABLE `t_data` ENABLE KEYS */;


-- Dumping structure for table sukubunga_bri.t_display
DROP TABLE IF EXISTS `t_display`;
CREATE TABLE IF NOT EXISTS `t_display` (
  `bungax` int(11) default NULL,
  `kursx` int(11) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table sukubunga_bri.t_display: 1 rows
/*!40000 ALTER TABLE `t_display` DISABLE KEYS */;
REPLACE INTO `t_display` (`bungax`, `kursx`) VALUES
	(2, 1);
/*!40000 ALTER TABLE `t_display` ENABLE KEYS */;


-- Dumping structure for table sukubunga_bri.t_kurs
DROP TABLE IF EXISTS `t_kurs`;
CREATE TABLE IF NOT EXISTS `t_kurs` (
  `nox` int(5) default NULL,
  `negara` varchar(25) default NULL,
  `beli` decimal(20,2) default NULL,
  `jual` decimal(20,2) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table sukubunga_bri.t_kurs: 6 rows
/*!40000 ALTER TABLE `t_kurs` DISABLE KEYS */;
REPLACE INTO `t_kurs` (`nox`, `negara`, `beli`, `jual`) VALUES
	(1, 'EUR', 10394.00, 11133.00),
	(2, 'SGD', 17069.00, 18389.00),
	(3, 'GBP', 18720.00, 19509.00),
	(4, 'USD', 1734.10, 1948.70),
	(5, 'JPY', 137.50, 146.01),
	(6, 'MYR', 9724.00, 10171.00);
/*!40000 ALTER TABLE `t_kurs` ENABLE KEYS */;


-- Dumping structure for table sukubunga_bri.t_shutdown
DROP TABLE IF EXISTS `t_shutdown`;
CREATE TABLE IF NOT EXISTS `t_shutdown` (
  `matikan` int(11) default NULL,
  `ket` int(11) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table sukubunga_bri.t_shutdown: 1 rows
/*!40000 ALTER TABLE `t_shutdown` DISABLE KEYS */;
REPLACE INTO `t_shutdown` (`matikan`, `ket`) VALUES
	(0, 0);
/*!40000 ALTER TABLE `t_shutdown` ENABLE KEYS */;


-- Dumping structure for table sukubunga_bri.t_simpanan
DROP TABLE IF EXISTS `t_simpanan`;
CREATE TABLE IF NOT EXISTS `t_simpanan` (
  `kd_simpanan` int(11) default NULL,
  `jenis` varchar(50) default NULL,
  `nama` varchar(50) default NULL,
  `bunga` decimal(20,2) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table sukubunga_bri.t_simpanan: 24 rows
/*!40000 ALTER TABLE `t_simpanan` DISABLE KEYS */;
REPLACE INTO `t_simpanan` (`kd_simpanan`, `jenis`, `nama`, `bunga`) VALUES
	(1, 'deposito1', '1 - 2 Bulan', 3.50),
	(2, 'deposito2', '3 Bulan', 3.50),
	(3, 'deposito3', '6 Bulan', 3.50),
	(4, 'deposito4', '12 Bulan', 3.50),
	(5, 'deposito5', '18 Bulan', 3.50),
	(6, 'deposito6', '24 Bulan', 3.50),
	(7, 'britama1', '0 s/d < 1 JT', 0.00),
	(8, 'britama2', '>= 1 JT s/d < 50 JT', 0.35),
	(9, 'britama3', '>= 50 JT s/d < 500 JT', 0.50),
	(10, 'britama4', '>= 500 JT s/d < 1M', 1.00),
	(11, 'britama5', '>= 1 M', 1.50),
	(12, 'britama6', '-', 0.00),
	(13, 'junio1', '0 s/d < 1 JT', 0.00),
	(14, 'junio2', '>= 1 JT s/d < 50 JT', 0.50),
	(15, 'junio3', '>= 50 JT s/d < 500 JT', 0.50),
	(16, 'junio4', '>= 500 JT s/d < 1M', 1.00),
	(17, 'junio5', '>= 1 M', 1.50),
	(18, 'junio6', '-', 0.00),
	(19, 'giro1', '0 s/d s/d 5 JT', 0.00),
	(20, 'giro2', '5 JT s/d 25 JT', 0.25),
	(21, 'giro3', '> 25 JT s/d 100 JT', 0.25),
	(22, 'giro4', '> 100 JT s/d 1 M', 1.00),
	(23, 'giro5', '> 1 M', 1.00),
	(24, 'giro6', '-', 0.00);
/*!40000 ALTER TABLE `t_simpanan` ENABLE KEYS */;


-- Dumping structure for table sukubunga_bri.t_tnt
DROP TABLE IF EXISTS `t_tnt`;
CREATE TABLE IF NOT EXISTS `t_tnt` (
  `tnt` int(11) default NULL,
  `ket1` int(11) default NULL,
  `ket2` int(11) default NULL,
  `ket3` int(11) default NULL,
  `ket4` int(11) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table sukubunga_bri.t_tnt: 1 rows
/*!40000 ALTER TABLE `t_tnt` DISABLE KEYS */;
REPLACE INTO `t_tnt` (`tnt`, `ket1`, `ket2`, `ket3`, `ket4`) VALUES
	(0, 0, 0, 0, 0);
/*!40000 ALTER TABLE `t_tnt` ENABLE KEYS */;


-- Dumping structure for table sukubunga_bri.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `username` varchar(20) default NULL,
  `password` varchar(32) default NULL,
  `level` varchar(10) default NULL,
  `bagian` varchar(20) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table sukubunga_bri.user: 10 rows
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
REPLACE INTO `user` (`username`, `password`, `level`, `bagian`) VALUES
	('teller1', 'e10adc3949ba59abbe56e057f20f883e', 'teller', 'Teller 1'),
	('teller2', 'e10adc3949ba59abbe56e057f20f883e', 'teller', 'Teller 2'),
	('teller3', 'e10adc3949ba59abbe56e057f20f883e', 'teller', 'Teller 3'),
	('teller4', 'e10adc3949ba59abbe56e057f20f883e', 'teller', 'Teller 4'),
	('teller5', 'e10adc3949ba59abbe56e057f20f883e', 'teller', 'Teller 5'),
	('cs1', 'e10adc3949ba59abbe56e057f20f883e', 'cs', 'Customer Service 1'),
	('cs2', 'e10adc3949ba59abbe56e057f20f883e', 'cs', 'Customer Service 2'),
	('cs3', 'e10adc3949ba59abbe56e057f20f883e', 'cs', 'Customer Service 3'),
	('cs4', 'e10adc3949ba59abbe56e057f20f883e', 'cs', 'Customer Service 4'),
	('cs5', 'e10adc3949ba59abbe56e057f20f883e', 'cs', 'Customer Service 5');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
