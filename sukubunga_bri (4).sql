-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 11, 2020 at 01:45 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.3.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sukubunga_bri`
--

-- --------------------------------------------------------

--
-- Table structure for table `cek_error`
--

CREATE TABLE `cek_error` (
  `jmlh_error` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cek_error`
--

INSERT INTO `cek_error` (`jmlh_error`) VALUES
(0);

-- --------------------------------------------------------

--
-- Table structure for table `setvol`
--

CREATE TABLE `setvol` (
  `volx` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setvol`
--

INSERT INTO `setvol` (`volx`) VALUES
(10);

-- --------------------------------------------------------

--
-- Table structure for table `t_amandemen`
--

CREATE TABLE `t_amandemen` (
  `id` int(11) NOT NULL,
  `no_amandemen` varchar(255) DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_amandemen`
--

INSERT INTO `t_amandemen` (`id`, `no_amandemen`, `createdby`, `createddate`) VALUES
(1, 'dep-10-11', 11, '2020-11-10 14:16:37'),
(2, 'dep-11-11', 11, '2020-11-10 14:17:51');

-- --------------------------------------------------------

--
-- Table structure for table `t_data`
--

CREATE TABLE `t_data` (
  `id` int(11) NOT NULL,
  `uker` varchar(150) DEFAULT NULL,
  `almt` varchar(75) DEFAULT NULL,
  `tlp` varchar(25) DEFAULT NULL,
  `link` varchar(50) DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_data`
--

INSERT INTO `t_data` (`id`, `uker`, `almt`, `tlp`, `link`, `createdby`, `createddate`, `updateddate`, `updatedby`) VALUES
(1, 'BRI Unit Sidoarjo Kota 1', 'Sidoarjo', '-', 'SID', NULL, NULL, '2020-11-10 13:19:51', 11),
(2, 'BRI Gresik', 'Gresi', '-', 'GSK', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_display`
--

CREATE TABLE `t_display` (
  `id` int(11) NOT NULL,
  `bungax` int(11) DEFAULT NULL,
  `kursx` int(11) DEFAULT NULL,
  `published` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_display`
--

INSERT INTO `t_display` (`id`, `bungax`, `kursx`, `published`) VALUES
(1, 2, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_interval`
--

CREATE TABLE `t_interval` (
  `id` int(11) NOT NULL,
  `waktu` int(11) NOT NULL,
  `ket` varchar(150) NOT NULL,
  `createdby` int(11) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_interval`
--

INSERT INTO `t_interval` (`id`, `waktu`, `ket`, `createdby`, `createddate`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 120000, 'milisecond', NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_interval_info`
--

CREATE TABLE `t_interval_info` (
  `id` int(11) NOT NULL,
  `interval` int(11) NOT NULL,
  `createdby` int(11) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_interval_info`
--

INSERT INTO `t_interval_info` (`id`, `interval`, `createdby`, `createddate`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 3000, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_ket_simpanan`
--

CREATE TABLE `t_ket_simpanan` (
  `id` int(11) NOT NULL,
  `ket` varchar(255) DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_ket_simpanan`
--

INSERT INTO `t_ket_simpanan` (`id`, `ket`, `createdby`, `createddate`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'DEPOSITO', 11, '2020-11-10 13:39:27', NULL, NULL, 0),
(2, 'BRITAMA', 11, '2020-11-10 13:41:47', NULL, NULL, 0),
(3, 'JUNIO', 11, '2020-11-10 13:43:46', NULL, NULL, 0),
(4, 'GIRO', 11, '2020-11-10 13:44:05', '2020-11-10 13:46:35', 11, 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_kurs`
--

CREATE TABLE `t_kurs` (
  `nox` int(5) NOT NULL,
  `negara` varchar(25) DEFAULT NULL,
  `beli` decimal(20,2) DEFAULT NULL,
  `jual` decimal(20,2) DEFAULT NULL,
  `kurs_no` int(11) DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_kurs`
--

INSERT INTO `t_kurs` (`nox`, `negara`, `beli`, `jual`, `kurs_no`, `createdby`, `createddate`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'EUR', '10394.00', '11133.00', 1, NULL, NULL, '2020-11-11 06:14:07', 11, 0),
(2, 'SGD', '17069.00', '18389.00', 1, NULL, NULL, NULL, NULL, 0),
(3, 'GBP', '18720.00', '19509.00', 1, NULL, NULL, NULL, NULL, 0),
(4, 'USD', '1734.10', '1948.70', 1, NULL, NULL, NULL, NULL, 0),
(5, 'JPY', '137.50', '146.01', 1, NULL, NULL, NULL, NULL, 0),
(6, 'MYR', '9724.00', '10171.00', 1, NULL, NULL, NULL, NULL, 0),
(7, 'EUR', '10394.00', '11133.00', NULL, 11, '2020-11-11 06:10:50', NULL, NULL, 0),
(8, 'EUR', '10394.00', '11133.00', NULL, 11, '2020-11-11 06:11:50', '2020-11-11 06:14:13', 11, 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_shutdown`
--

CREATE TABLE `t_shutdown` (
  `matikan` int(11) DEFAULT NULL,
  `ket` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_shutdown`
--

INSERT INTO `t_shutdown` (`matikan`, `ket`) VALUES
(0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_simpanan`
--

CREATE TABLE `t_simpanan` (
  `id` int(11) NOT NULL,
  `kd_simpanan` int(11) DEFAULT NULL,
  `jenis` varchar(50) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `bunga` decimal(20,2) DEFAULT NULL,
  `display_no` int(11) DEFAULT NULL,
  `ket` varchar(150) DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `t_amandemen` int(11) DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_simpanan`
--

INSERT INTO `t_simpanan` (`id`, `kd_simpanan`, `jenis`, `nama`, `bunga`, `display_no`, `ket`, `createdby`, `createddate`, `updateddate`, `updatedby`, `t_amandemen`, `deleted`) VALUES
(1, 1, 'deposito1', '1 - 2 Bulan', '3.50', 1, 'DEPOSITO', NULL, NULL, '2020-11-10 14:17:51', 11, 2, 0),
(2, 2, 'deposito1', '1 - 2 Bulan', '3.50', 1, 'DEPOSITO', NULL, NULL, '2020-11-10 14:17:51', 11, 2, 0),
(3, 3, 'deposito1', '1 - 2 Bulan', '3.50', 1, 'DEPOSITO', NULL, NULL, '2020-11-10 14:17:51', 11, 2, 0),
(4, 4, 'deposito1', '1 - 2 Bulan', '3.50', 1, 'DEPOSITO', NULL, NULL, '2020-11-10 14:17:51', 11, 2, 0),
(5, 5, 'deposito1', '1 - 2 Bulan', '3.50', 1, 'DEPOSITO', NULL, NULL, '2020-11-10 14:17:51', 11, 2, 0),
(6, 6, 'deposito1', '1 - 2 Bulan', '3.50', 1, 'DEPOSITO', NULL, NULL, '2020-11-10 14:17:51', 11, 2, 0),
(7, 7, 'britama1', '0 s/d < 1 JT', '0.00', 2, 'BRITAMA', NULL, NULL, NULL, NULL, NULL, 0),
(8, 8, 'britama2', '>= 1 JT s/d < 50 JT', '0.35', 2, 'BRITAMA', NULL, NULL, NULL, NULL, NULL, 0),
(9, 9, 'britama3', '>= 50 JT s/d < 500 JT', '0.50', 2, 'BRITAMA', NULL, NULL, NULL, NULL, NULL, 0),
(10, 10, 'britama4', '>= 500 JT s/d < 1M', '1.00', 2, 'BRITAMA', NULL, NULL, NULL, NULL, NULL, 0),
(11, 11, 'britama5', '>= 1 M', '1.50', 2, 'BRITAMA', NULL, NULL, NULL, NULL, NULL, 0),
(12, 12, 'britama6', '-', '0.00', 2, 'BRITAMA', NULL, NULL, NULL, NULL, NULL, 0),
(13, 13, 'junio1', '0 s/d < 1 JT', '0.00', 3, 'JUNIO', NULL, NULL, NULL, NULL, NULL, 0),
(14, 14, 'junio2', '>= 1 JT s/d < 50 JT', '0.50', 3, 'JUNIO', NULL, NULL, NULL, NULL, NULL, 0),
(15, 15, 'junio3', '>= 50 JT s/d < 500 JT', '0.50', 3, 'JUNIO', NULL, NULL, NULL, NULL, NULL, 0),
(16, 16, 'junio4', '>= 500 JT s/d < 1M', '1.00', 3, 'JUNIO', NULL, NULL, NULL, NULL, NULL, 0),
(17, 17, 'junio5', '>= 1 M', '1.50', 3, 'JUNIO', NULL, NULL, NULL, NULL, NULL, 0),
(18, 18, 'junio6', '-', '0.00', 3, 'JUNIO', NULL, NULL, NULL, NULL, NULL, 0),
(19, 19, 'giro1', '0 s/d s/d 5 JT', '0.00', 4, 'GIRO', NULL, NULL, NULL, NULL, NULL, 0),
(20, 20, 'giro2', '5 JT s/d 25 JT', '0.25', 4, 'GIRO', NULL, NULL, NULL, NULL, NULL, 0),
(21, 21, 'giro3', '> 25 JT s/d 100 JT', '0.25', 4, 'GIRO', NULL, NULL, NULL, NULL, NULL, 0),
(22, 22, 'giro4', '> 100 JT s/d 1 M', '1.00', 4, 'GIRO', NULL, NULL, NULL, NULL, NULL, 0),
(23, 23, 'giro5', '> 1 M', '1.00', 4, 'GIRO', NULL, NULL, NULL, NULL, NULL, 0),
(24, 24, 'giro6', '-', '0.00', 4, 'GIRO', NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_tnt`
--

CREATE TABLE `t_tnt` (
  `tnt` int(11) DEFAULT NULL,
  `ket1` int(11) DEFAULT NULL,
  `ket2` int(11) DEFAULT NULL,
  `ket3` int(11) DEFAULT NULL,
  `ket4` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_tnt`
--

INSERT INTO `t_tnt` (`tnt`, `ket1`, `ket2`, `ket3`, `ket4`) VALUES
(0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_video`
--

CREATE TABLE `t_video` (
  `id` int(11) NOT NULL,
  `video` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_video`
--

INSERT INTO `t_video` (`id`, `video`) VALUES
(1, 'bri_video.mp4');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `level` varchar(10) DEFAULT NULL,
  `bagian` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `level`, `bagian`) VALUES
(1, 'teller1', 'e10adc3949ba59abbe56e057f20f883e', 'teller', 'Teller 1'),
(2, 'teller2', 'e10adc3949ba59abbe56e057f20f883e', 'teller', 'Teller 2'),
(3, 'teller3', 'e10adc3949ba59abbe56e057f20f883e', 'teller', 'Teller 3'),
(4, 'teller4', 'e10adc3949ba59abbe56e057f20f883e', 'teller', 'Teller 4'),
(5, 'teller5', 'e10adc3949ba59abbe56e057f20f883e', 'teller', 'Teller 5'),
(6, 'cs1', 'e10adc3949ba59abbe56e057f20f883e', 'cs', 'Customer Service 1'),
(7, 'cs2', 'e10adc3949ba59abbe56e057f20f883e', 'cs', 'Customer Service 2'),
(8, 'cs3', 'e10adc3949ba59abbe56e057f20f883e', 'cs', 'Customer Service 3'),
(9, 'cs4', 'e10adc3949ba59abbe56e057f20f883e', 'cs', 'Customer Service 4'),
(10, 'cs5', 'e10adc3949ba59abbe56e057f20f883e', 'cs', 'Customer Service 5'),
(11, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 'IT');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_amandemen`
--
ALTER TABLE `t_amandemen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_data`
--
ALTER TABLE `t_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_display`
--
ALTER TABLE `t_display`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_interval`
--
ALTER TABLE `t_interval`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_interval_info`
--
ALTER TABLE `t_interval_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_ket_simpanan`
--
ALTER TABLE `t_ket_simpanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kurs`
--
ALTER TABLE `t_kurs`
  ADD PRIMARY KEY (`nox`);

--
-- Indexes for table `t_simpanan`
--
ALTER TABLE `t_simpanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_video`
--
ALTER TABLE `t_video`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_amandemen`
--
ALTER TABLE `t_amandemen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `t_data`
--
ALTER TABLE `t_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `t_display`
--
ALTER TABLE `t_display`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_interval`
--
ALTER TABLE `t_interval`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_interval_info`
--
ALTER TABLE `t_interval_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_ket_simpanan`
--
ALTER TABLE `t_ket_simpanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `t_kurs`
--
ALTER TABLE `t_kurs`
  MODIFY `nox` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `t_simpanan`
--
ALTER TABLE `t_simpanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `t_video`
--
ALTER TABLE `t_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
